# Sapient Games Arena

## Development Build

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## Github - 
https://bitbucket.org/modimrugesh1910/sapient-games-arena/src

## Activities

* fetching data over http call
* sharing data over files
* sorting functionality
* searching functionality
* pagination
* theme changing
* version control

