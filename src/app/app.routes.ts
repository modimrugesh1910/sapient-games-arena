import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {NgModule} from '@angular/core';
import {FeatureTableComponent} from './feature-table/feature-table.component';

const routes: Routes = [
    {
        path : '',
        redirectTo: 'feature-table',
        pathMatch: 'full',
    },
    {
        path : '*',
        redirectTo: 'feature-table',
        pathMatch: 'full',
    },
    {
        path : 'feature-table',
        component: FeatureTableComponent
    }
];

/** Router module for root URL component */
export const AppRoutes = RouterModule.forRoot(routes, {
    useHash: true
});
